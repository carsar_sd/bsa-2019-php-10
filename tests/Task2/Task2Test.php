<?php

namespace Tests\Task2;

use App\Entities\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface as ProductRepository;
use App\Repositories\Interfaces\UserRepositoryInterface as UserRepository;
use App\Services\MarketService;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class Task2Test extends TestCase
{
    private function generateProducts( $productsCount, $usersCount)
    {
        $faker = Faker::create();
        $data = [];

        for ($i = 0; $i < $productsCount; $i++) {
            $data[] = [
                'id' =>  ($i + 1),
                'name' => $faker->name,
                'price' => $faker->randomFloat(2, 10, 10000),
                'user_id' => $faker->numberBetween(1, $usersCount),
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ];
        }

        return new Collection($data);
    }

    private function generateUsers($usersCount)
    {
        $faker = Faker::create();
        $data = [];

        for ($i = 0; $i < $usersCount; $i++) {
            $data[] = [
                'id' =>  ($i + 1),
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'email_verified_at' => now()->toDateTimeString(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ];
        }

        return new Collection($data);
    }

	public function test_get_product_list()
	{
	    $testProducts = $this->generateProducts(5, 2);
	    $testUsers    = $this->generateUsers(2);

        $productRepositoryStub = $this->createMock(ProductRepository::class);
        $productRepositoryStub->method('findAll')->willReturn($testProducts);

        $userRepositoryStub    = $this->createMock(UserRepository::class);

        $marketService = new MarketService($productRepositoryStub, $userRepositoryStub);
        $methodResult  = $marketService->getProductList();

        foreach ($methodResult as $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('name', $item);
            $this->assertArrayHasKey('price', $item);
            $this->assertArrayHasKey('user_id', $item);
            $this->assertArrayHasKey('created_at', $item);
            $this->assertArrayHasKey('updated_at', $item);
        }

        $this->assertInstanceOf(Collection::class, $methodResult);
        $this->assertEquals($testProducts, $methodResult);
	}

	public function test_get_product_by_id()
	{
        $testProductId = 1;
        $testProducts  = $this->generateProducts(5, 2);
        $testUsers     = $this->generateUsers(2);

        $productRepositoryStub = $this->createMock(ProductRepository::class);
        $productRepositoryStub->method('findById')->willReturn(new Product($testProducts->first()));

        $userRepositoryStub    = $this->createMock(UserRepository::class);

        $marketService = new MarketService($productRepositoryStub, $userRepositoryStub);
        $methodResult  = $marketService->getProductById($testProductId);
        $methodResultArray = $methodResult->toArray();

        $this->assertArrayHasKey('id', $methodResultArray);
        $this->assertArrayHasKey('name', $methodResultArray);
        $this->assertArrayHasKey('price', $methodResultArray);
        $this->assertArrayHasKey('user_id', $methodResultArray);

        $this->assertInstanceOf(Product::class, $methodResult);
        $this->assertEquals(new Product($testProducts->first()), $methodResult);
	}

    public function test_get_products_by_user_id()
    {
        $testUserId   = 1;
        $testProducts = $this->generateProducts(5, 1);
        $testUsers    = $this->generateUsers(2);

        $productRepositoryStub = $this->createMock(ProductRepository::class);
        $productRepositoryStub->method('findByUserId')->willReturn($testProducts);

        $userRepositoryStub    = $this->createMock(UserRepository::class);

        $marketService = new MarketService($productRepositoryStub, $userRepositoryStub);
        $methodResult  = $marketService->getProductsByUserId($testUserId);

        foreach ($methodResult as $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('name', $item);
            $this->assertArrayHasKey('price', $item);
            $this->assertArrayHasKey('user_id', $item);
            $this->assertArrayHasKey('created_at', $item);
            $this->assertArrayHasKey('updated_at', $item);
        }

        $this->assertInstanceOf(Collection::class, $methodResult);
        $this->assertEquals($testProducts, $methodResult);
    }

    public function test_store_product()
    {
        $testStoreData = [
            'id'      => 10,
            'user_id' => 1,
            'name'    => 'Test name',
            'price'   => 122.11,
        ];
        $testProducts  = $this->generateProducts(5, 2);
        $testUsers     = $this->generateUsers(2);

        $productRepositoryStub = $this->createMock(ProductRepository::class);
        $productRepositoryStub->method('store')->willReturn(new Product($testStoreData));

        $userRepositoryStub    = $this->createMock(UserRepository::class);
        $userRepositoryStub->method('findById')->willReturn($testUsers->first());

        $marketService = new MarketService($productRepositoryStub, $userRepositoryStub);
        $methodResult  = $marketService->storeProduct(Input::replace($testStoreData));
        $methodResultArray = $methodResult->toArray();

        $this->assertArrayHasKey('id', $methodResultArray);
        $this->assertArrayHasKey('name', $methodResultArray);
        $this->assertArrayHasKey('price', $methodResultArray);
        $this->assertArrayHasKey('user_id', $methodResultArray);

        $this->assertInstanceOf(Product::class, $methodResult);
    }

    public function test_update_product()
    {
        $testProductId  = 1;
        $testUpdateData = [
            'user_id' => 1,
            'name'    => 'Test name',
            'price'   => 122.11,
        ];

        $testProducts  = $this->generateProducts(5, 1);
        $testUsers     = $this->generateUsers(2);

        $productRepositoryStub = $this->createMock(ProductRepository::class);
        $productRepositoryStub->method('update')->willReturn(new Product(array_replace($testProducts->first(), $testUpdateData)));
        $productRepositoryStub->method('findById')->willReturn(new Product($testProducts->first()));

        $userRepositoryStub    = $this->createMock(UserRepository::class);
        $userRepositoryStub->method('findById')->willReturn($testUsers->first());

        $marketService = new MarketService($productRepositoryStub, $userRepositoryStub);
        $methodResult  = $marketService->updateProduct($testProductId, Input::replace($testUpdateData));
        $methodResultArray = $methodResult->toArray();

        $this->assertArrayHasKey('id', $methodResultArray);
        $this->assertArrayHasKey('name', $methodResultArray);
        $this->assertArrayHasKey('price', $methodResultArray);
        $this->assertArrayHasKey('user_id', $methodResultArray);

        $this->assertInstanceOf(Product::class, $methodResult);
    }

    public function test_delete_product()
    {
        $testProductId  = 1;
        $testDeleteData = [
            'user_id' => 1,
        ];

        $testProducts  = $this->generateProducts(5, 1);
        $testUsers     = $this->generateUsers(2);

        $productRepositoryStub = $this->createMock(ProductRepository::class);
        $productRepositoryStub->method('delete')->willReturn(true);
        $productRepositoryStub->method('findById')->willReturn(new Product($testProducts->first()));

        $userRepositoryStub    = $this->createMock(UserRepository::class);
        $userRepositoryStub->method('findById')->willReturn($testUsers->first());

        $marketService = new MarketService($productRepositoryStub, $userRepositoryStub);
        $methodResult  = $marketService->deleteProduct($testProductId, Input::replace($testDeleteData));

        $this->assertTrue($methodResult);
    }
}
