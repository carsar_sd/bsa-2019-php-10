<?php

namespace Tests\Task3;

use Tests\MigrateFreshSeedOnce;
use Tests\TestCase;

class Task3Test extends TestCase
{
    use MigrateFreshSeedOnce;

	public function test_get_products_list()
	{
		$data = $this->json('get', 'api/items')
			->assertHeader('Content-Type', 'application/json')
			->assertStatus(200)
			->assertJsonStructure([
				'*' => [
                    'id',
                    'name',
                    'price',
                    'user_id',
                    'user_name'
				]
			])
            ->json();

		foreach ($data as $item) {
			$this->assertArrayHasKey('id', $item);
			$this->assertArrayHasKey('name', $item);
			$this->assertArrayHasKey('price', $item);
			$this->assertArrayHasKey('user_id', $item);
			$this->assertArrayHasKey('user_name', $item);
		}
	}

	public function test_get_product_one_success()
	{
		$this->json('get', '/api/items/1')
			->assertHeader('Content-Type', 'application/json')
			->assertStatus(200)
			->assertJsonStructure([
				'id',
				'name',
				'price',
				'user_id',
				'user_name'
			]);
	}

    public function test_get_product_one_fail()
    {
        $data = $this->json('get', '/api/items/100')
            ->assertHeader('Content-Type', 'text/html; charset=UTF-8')
            ->assertStatus(404)
            ->content();

        $this->assertNotEmpty($data);
        $this->assertEquals('Product not found!', $data);
    }

    public function test_get_product_store_success()
    {
        $data = [
            'name' => 'Test Name 1',
            'price' => 123,
            'user_id' => 1,
        ];

        $this->post('/api/items/', $data)
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(201)
            ->assertJsonStructure([
                'id',
                'name',
                'price',
                'user_id',
                'user_name'
            ]);
    }

    public function test_get_product_store_fail()
    {
        $data = [
            'name' => 'Test Name 2',
            'price' => 321,
            'user_id' => 1000,
        ];

        $data = $this->post('/api/items/', $data)
            ->assertHeader('Content-Type', 'text/html; charset=UTF-8')
            ->assertStatus(404)
            ->content();

        $this->assertNotEmpty($data);
        $this->assertEquals('There is no users with such ID!', $data);
    }

    public function test_get_product_update_success()
    {
        $data = [
            'name'    => 'Apple MacBookPro',
            'price'   => 2000,
            'user_id' => 1,
        ];

        $this->put('/api/items/1', $data)
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'name',
                'price',
                'user_id',
                'user_name'
            ]);
    }

    public function test_get_product_update_fail()
    {
        $data = [
            'name'    => 'Apple MacBookPro',
            'price'   => 2000,
            'user_id' => 100,
        ];

        $data = $this->put('/api/items/1', $data)
            ->assertHeader('Content-Type', 'text/html; charset=UTF-8')
            ->assertStatus(404)
            ->content();

        $this->assertNotEmpty($data);
        $this->assertEquals('There is no users with such ID!', $data);
    }

    public function test_get_product_update_fail2()
    {
        $data = [
            'name'    => 'Apple MacBookPro',
            'price'   => 2000,
            'user_id' => 2,
        ];

        $data = $this->put('/api/items/1', $data)
            ->assertHeader('Content-Type', 'text/html; charset=UTF-8')
            ->assertStatus(403)
            ->content();

        $this->assertNotEmpty($data);
        $this->assertEquals('You can update only your own products!', $data);
    }

    public function test_get_product_delete_success()
    {
        $data = [
            'user_id' => 1,
        ];

        $data = $this->delete('/api/items/1', $data)
            ->assertStatus(200)
            ->original;

        $this->assertNotEmpty($data);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('result', $data);
        $this->assertEquals('success', $data['result']);
    }

    public function test_get_product_delete_fail()
    {
        $data = [
            'user_id' => 2,
        ];

        $data = $this->delete('/api/items/2', $data)
            ->assertHeader('Content-Type', 'text/html; charset=UTF-8')
            ->assertStatus(403)
            ->content();

        $this->assertNotEmpty($data);
        $this->assertEquals('You can remove only your own products!', $data);
    }
}
