<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** AUTH **/
Auth::routes();

Route::get('/logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);


/** ADMIN **/
Route::prefix('admin')
    ->middleware('auth')
    ->group(
        function() {
            Route::get('/', [
                'as' => 'admin',
                'uses' => 'Admin\IndexController@index'
            ]);
            Route::get('/products/add', [
                'as' => 'product_add_form',
                'uses' => 'Admin\ProductsController@add_form'
            ]);
            Route::post('/products', [
                'as' => 'product_store',
                'uses' => 'Admin\ProductsController@store'
            ]);
            Route::delete('/products/{id}', [
                'as' => 'product_delete',
                'uses' => 'Admin\ProductsController@delete'
            ])
                ->where('id', '[0-9]+');
        }
    );


/** PRODUCTS **/
Route::get('/', [
    'as' => 'market',
    'uses' => 'MarketWebController@market'
]);
Route::get('/items/{id}', [
    'as' => 'product_show',
    'uses' => 'MarketWebController@product'
])
    ->where('id', '[0-9]+');
