@extends('layouts.product')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            {{ $product->name }}
        </div>
        <div class="sub_title m-b-md">
            {{ $product->price }} $
        </div>

        <a href="{{ url()->previous() }}">Back</a>
    </div>
@endsection