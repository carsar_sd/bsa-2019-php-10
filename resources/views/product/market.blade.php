@extends('layouts.product')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            {{ config('app.name', 'Laravel') }}
        </div>

        <ul>
            @foreach($products as $product)
                <li class="list-items">
                    <a href="{{ route('product_show', ['id' => $product->id]) }}">
                        {{ $product->name }} : {{ $product->price }} $
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
