@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 align-items-center">
                <form method="POST" action="{{ route('product_store') }}">
                    @csrf
                    <div style="display: inline-block; margin-right: 10px;">
                        <label for="product_name">Product name:</label><br>
                        <input id="product_name" name="name" type="text" value="{{ old('name') }}" required>
                    </div>
                    <div style="display: inline-block;  margin-left: 10px;">
                        <label for="product_price">Product price:</label><br>
                        <input id="product_price" name="price" type="number" step="0.01" min="0" value="{{ old('price') }}" required>
                    </div>

                    <div style="display: inline-block;  margin-left: 10px; vertical-align: bottom;">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection