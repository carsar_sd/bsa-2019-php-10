<?php

namespace App\Services;

use Auth;
use App\Entities\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface as ProductRepository;
use App\Repositories\Interfaces\UserRepositoryInterface as UserRepository;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\Mime\Exception\InvalidArgumentException;

class MarketService
{
    private $productRepository;
    private $userRepository;

    public function __construct(ProductRepository $productRepository, UserRepository $userRepository)
    {
        $this->productRepository = $productRepository;
        $this->userRepository = $userRepository;
    }

    public function getProductList(): Collection
    {
        return $this->productRepository->findAll();
    }

    public function getProductById(int $id): ?Product
    {
        $product = $this->productRepository->findById($id);

        if (!$product) {
            throw new InvalidArgumentException('Product not found!', Response::HTTP_NOT_FOUND);
        }

        return $product;
    }

    public function getProductsByUserId(int $userId): Collection
    {
        return $this->productRepository->findByUserId($userId);
    }

    public function storeProduct(): Product
    {
        $data    = Input::all();

        $user_id = Auth::user() ? Auth::user()->id : $data['user_id'];

        if (!$user_id) {
            throw new InvalidArgumentException('Only authorised users can store products!', Response::HTTP_FORBIDDEN);
        }

        $user = $this->userRepository->findById($user_id);

        if (!$user) {
            throw new InvalidArgumentException('There is no users with such ID!', Response::HTTP_NOT_FOUND);
        }

        $product = new Product([
            'user_id' => $user_id,
            'name'    => $data['name'],
            'price'   => $data['price']
        ]);

        return $this->productRepository->store($product);
    }

    public function updateProduct($id): Product
    {
        $data    = Input::all();
        $user_id = Auth::user() ? Auth::user()->id : $data['user_id'];

        if (!$user_id) {
            throw new InvalidArgumentException('Only authorised users can store products!', Response::HTTP_FORBIDDEN);
        }

        $user = $this->userRepository->findById($user_id);

        if (!$user) {
            throw new InvalidArgumentException('There is no users with such ID!', Response::HTTP_NOT_FOUND);
        }

        $product = $this->getProductById($id);

        if ($product->user_id != $user_id) {
            throw new InvalidArgumentException('You can update only your own products!', Response::HTTP_FORBIDDEN);
        }

        foreach ($data as $key => $value) {
            $product->$key = $value;
        }

        return $this->productRepository->update($product);
    }

    public function deleteProduct($id): ?bool
    {
        $data    = Input::all();
        $user_id = Auth::user() ? Auth::user()->id : $data['user_id'];

        if (!$user_id) {
            throw new InvalidArgumentException('Only authorised users can store products!', Response::HTTP_FORBIDDEN);
        }

        $user = $this->userRepository->findById($user_id);

        if (!$user) {
            throw new InvalidArgumentException('There is no users with such ID!', Response::HTTP_NOT_FOUND);
        }

        $product = $this->getProductById($id);

        if (!$product) {
            throw new InvalidArgumentException('Product not found!', Response::HTTP_NOT_FOUND);
        }

        if ($product->user_id != $user_id) {
            throw new InvalidArgumentException('You can remove only your own products!', Response::HTTP_FORBIDDEN);
        }

        return $this->productRepository->delete($product);
    }
}
