<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\MarketService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductsController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function add_form()
    {
        return view('admin.product_add_form');
    }

    public function store(Request $request)
    {
        try {
            $this->marketService->storeProduct($request);

            return redirect()->route('admin');
        } catch (\Exception $e) {
            return new Response($e->getMessage(), $e->getCode());
        }
    }

    public function delete(int $id)
    {
        try {
            $this->marketService->deleteProduct($id);

            return redirect()->route('admin');
        } catch (\Exception $e) {
            return new Response($e->getMessage(), $e->getCode());
        }
    }
}
