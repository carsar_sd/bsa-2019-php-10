<?php

namespace App\Http\Controllers;

use App\Services\MarketService;

class MarketWebController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function market()
    {
        $products = $this->marketService->getProductList();

        return view('product.market', compact('products'));
    }

    public function product(int $id)
    {
        try {
            $product = $this->marketService->getProductById($id);

            return view('product.product', compact('product'));
        } catch (\Exception $e) {
            return redirect()->route('market');
        }
    }
}
