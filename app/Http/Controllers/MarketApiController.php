<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Services\MarketService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function index()
    {
        $products = $this->marketService->getProductList();

        return $products->map(function ($product) {
            return new ProductResource($product);
        });
    }

    public function show(int $id)
    {
        try {
            $product = $this->marketService->getProductById($id);

            return new ProductResource($product);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), $e->getCode());
        }
    }

    public function store(Request $request)
    {
        try {
            $product = $this->marketService->storeProduct($request);

            return new ProductResource($product);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), $e->getCode());
        }
    }

    public function update(int $id)
    {
        try {
            $product = $this->marketService->updateProduct($id);

            return new ProductResource($product);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), $e->getCode());
        }
    }

    public function destroy(int $id)
    {
        try {
            $this->marketService->deleteProduct($id);

            return ['result' => 'success'];
        } catch (\Exception $e) {
            return new Response($e->getMessage(), $e->getCode());
        }
    }
}
