<?php

namespace App\Repositories\Interfaces;

interface UserRepositoryInterface
{
    public function findAll();

    public function findById(int $id);
}