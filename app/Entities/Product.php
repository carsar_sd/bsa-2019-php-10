<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Entity
 * @property int $id
 * @property string $name
 * @property float $price
 * @property int $user_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property User $user
 */
class Product extends Model
{
    protected $table      = 'products';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'name', 'price', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getBuyer(): User
    {
        return $this->user;
    }
}
